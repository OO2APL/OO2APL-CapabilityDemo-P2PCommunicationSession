package demo;
  
import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentContextInterface;
import oo2apl.agent.AgentID;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.builtin.SubPlanInterface;
import p2pCommunicationCapability.CommunicationCapability;
import p2pCommunicationCapability.CommunicationSessionId;
import p2pCommunicationCapability.triggers.Quit;
import p2pCommunicationCapability.triggers.SessionInvitation;
import p2pCommunicationCapability.triggers.SessionOrganizeTrigger; 
/**
 * This agents demos a minimal setupt for using the P2P communication capability. The agent has to implement 
 * some plan schemes for handling the session events. 
 *  
 * @author Bas Testerink
 *
 */
public final class DemoAgent extends AgentBuilder {
	private final boolean acceptsInvitations;
	
	public DemoAgent(final boolean acceptsInvitations){
		this.acceptsInvitations = acceptsInvitations;
		
		// Setup the P2P communication capability
		include(new CommunicationCapability(
				DemoAgent::onOrganize, 
				this::onInvited, 
				DemoAgent::onAccept, 
				DemoAgent::onReject, 
				DemoAgent::onQuit));
		
		// Plan scheme to handle the invitation of peers, when so prompted
		addExternalTriggerPlanScheme(DemoAgent::invitePeers);
		
		// Plan scheme to handle the receipt of a greeting
		addMessagePlanScheme(DemoAgent::sayBye);
	}

	/** Adopts a SessionOrganizeTrigger that was received externally as an internal trigger. */
	private static final SubPlanInterface invitePeers(final Trigger trigger, final AgentContextInterface contextInterface){
		if(trigger instanceof SessionOrganizeTrigger){
			return (planInterface)->{
				planInterface.addInternalTrigger(trigger);
			};
		} else return SubPlanInterface.UNINSTANTIATED;
	}
	
	/** The agent says bye as a reaction to a greeting. */
	private static final SubPlanInterface sayBye(final Trigger trigger, final AgentContextInterface contextInterface){
		if(trigger instanceof Greeting){
			return (planInterface)->{ 
				Greeting greeting = (Greeting) trigger;
				System.out.println(planInterface.getAgentID()+": Bye "+greeting.getSender());
				planInterface.addInternalTrigger(new Quit(planInterface.getAgentID(), greeting.getSessionId()));
			};
		} else return SubPlanInterface.UNINSTANTIATED;
	}
	
	// Communication capability setup

	/** Predicate to implement in order to determine whether to accept an invitation. */
	private final boolean onInvited(final SessionInvitation invitation, final PlanToAgentInterface planInterface){
		return this.acceptsInvitations;
	}

	/** Plan to implement when the agent is organizing a session. Can be used to intialize some variables for the session for instance. */
	private static final void onOrganize(final SessionOrganizeTrigger organizeTrigger, final CommunicationSessionId sessionId, final PlanToAgentInterface planInterface){
		System.out.println(planInterface.getAgentID()+" is organizing session "+sessionId);
	}

	/** Plan to implement when a peer has accepted an invitation. */
	private static final void onAccept(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has accepted to participate in "+sessionId);
		planInterface.sendMessage(peer, new Greeting(planInterface.getAgentID(), sessionId));
		System.out.println(planInterface.getAgentID()+": Hi "+peer);
	}
	
	/** Plan to implement when an invitation is rejected. */
	private static final void onReject(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has rejected to participate in "+sessionId);
	}

	/** Plan to implement when a peer has quit a session. */
	private static final void onQuit(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface){ 
		System.out.println(peer+" has quit "+sessionId);
	}
}
