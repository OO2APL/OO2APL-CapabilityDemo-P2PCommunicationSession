package demo;

import oo2apl.agent.AgentID;
import oo2apl.agent.ExternalProcessToAgentInterface;
import oo2apl.defaults.messenger.DefaultMessenger;
import oo2apl.platform.AdminToPlatformInterface;
import oo2apl.platform.Platform;
import p2pCommunicationCapability.triggers.SessionOrganizeTrigger;
/**
 * This demo creates three agents. One agent organizes a communication session and invites 
 * therefore the other two. One peer will reject the invitation and the other accept it. The 
 * organizing agent says Hi to the accepting peer, who says Bye back. 
 * 
 * @author Bas Testerink
 */
public final class P2pCapabilityDemoMain {

	public final void demo(){
		// Make agents
		AdminToPlatformInterface platform = Platform.newPlatform(1, new DefaultMessenger());
		ExternalProcessToAgentInterface organizingAgent = platform.newAgent(new DemoAgent(true));
		ExternalProcessToAgentInterface acceptingAgent = platform.newAgent(new DemoAgent(true));
		ExternalProcessToAgentInterface rejectingAgent = platform.newAgent(new DemoAgent(false));
		
		// Invite the agents
		organizingAgent.addExternalTrigger(new SessionOrganizeTrigger(new AgentID[]{ acceptingAgent.getAgentID(), rejectingAgent.getAgentID() }));  
	}
	
	public final static void main(final String[] args){
		(new P2pCapabilityDemoMain()).demo();
	}
}
