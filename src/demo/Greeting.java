package demo;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;

public final class Greeting implements Trigger {
	private final AgentID sender;
	private final CommunicationSessionId sessionId;
	
	public Greeting(final AgentID sender, final CommunicationSessionId sessionId){
		this.sender = sender;
		this.sessionId = sessionId;
	}
	
	public final AgentID getSender(){ return this.sender; }
	public final CommunicationSessionId getSessionId(){ return this.sessionId; }
}
